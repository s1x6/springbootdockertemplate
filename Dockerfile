FROM openjdk:8-jdk-alpine
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/volumes/springdockertest-0.0.1-SNAPSHOT.jar"]