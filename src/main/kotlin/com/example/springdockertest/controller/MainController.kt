package com.example.springdockertest.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class MainController {

    @GetMapping("/hello")
    fun sayHello(): String {
        return "Hello world!1111!1!!!!111!"
    }

    data class Data(val text: String)

}