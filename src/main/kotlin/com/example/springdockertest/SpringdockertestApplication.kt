package com.example.springdockertest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringdockertestApplication

fun main(args: Array<String>) {
	runApplication<SpringdockertestApplication>(*args)
}
